# Clean Code in Python - Summary 

## Introduction 

what clean code is and what it means ? 


Before even answering these questions we can say for sure that it's a necessity. Without quality code, the project will face
the perils of failing due to an accumulation of technical debt. 

There is no sole or strict definition of clean code. There is also no way to metric to measure it. It's something that professionals decide. to understand what is clean code we have to understand that The real meaning of the "language" part of "programming languages" is to communicate our ideas to other developers. 

Clean code => code that is able to be easily understable by a new person that is discovering the code base and can modify/extend the code pretty easily


Advantages of clean code : maintainability, reducing technical debt, working effectively

If we want our project to successfully deliver features constantly at a steady and predictable pace, then having a good and maintainable code base is a must. 
Not
everything can be caught by automated tools, but whenever it's possible, it's
a good investment. The rest relies on good, thorough code reviews, and
good automated testing.

Software is only useful to the degree to which it can be easily changed.If the code can't
be changed, then it's useless. Having a
clean code base is an absolute requirement for it to be modified, hence the
importance of clean code


we write clean code because we want to achieve high
maintainability. If there's no need to maintain that code, then we can skip
the effort of maintaining high-quality standards on it.


### Code formating & Documentation 

A coding guideline is a bare minimum a project should have to be
considered being developed under quality standards.

If the code is not correct nor
consistently structured, and everyone on the team is doing things in their
own way, then we will end up with code that will require extra effort and
concentration to be understood. It will be error-prone, misleading, and bugs
or subtleties might slip through easily.

Ideally, there
should be a written document for the company or team you're working in
that explains the coding standard that's expected to be followed.


documenting code in Python, from within the code. One important distinction: documenting code is not the same as adding
comments to it.

As a general rule, we should aim to have as few code comments as possible. comments in code are a
symptom of our inability to express our code correctly.

there's another kind of comment in code that is definitely bad, and
there's just no way to justify it: commented out code. This code must be
deleted mercilessly.


In simple terms, we can say that docstrings are documentation embedded
in the source code. A docstring is basically a literal string, placed
somewhere in the code to document that part of the logic.

Maintaining proper documentation is a software engineering challenge that
we cannot escape from. It also makes sense for it to be like this. If you think
about it, the reason for documentation to be manually written is because it
is intended to be read by other humans.

PEP-3107 introduced the concept of annotations and PEP-484 checks the types of our functions via annotations.
Annotations bring a convenient way of writing classes in a compact way and defining small container objects.

We declare attributes in a class, and use annotations to set their type, and with
the help of the `@dataclass` decorator, they will be handled as instance
attributes

Do annotations replace docstrings? No, the documentation serves as valuable input, not only for
understanding and getting an idea of what is being passed around but also
as a valuable source for unit tests with data examples. 

The cost is that, as we mentioned earlier, it takes up a lot
of lines, and it needs to be verbose and detailed to be effective.

### Tools 

The idea of using tools is to have a repeatable and automatic way of
running certain checks. Therefore these tools should
be configured as part of the Continuous Integration (CI) build.

#### type consistency

Python is dynamically typed, but we can still add type
annotations to hint to the readers (and tools) about what to expect in
different parts of the code.

tools : mypy, pytype, pyright, pyre

#### Generic validation & Formating 

pylint / .pylintrc , validation of docstring, number of arguments, etc 
[DESIGN]
disable=missing-function-docstring

black : formats code in a unique and deterministic way
flake8 : linter 
isort : sort your imports (set profile to "black")



### Setup for automatic checks

Makefiles are powerful tools that let us configure
commands to be run in the project, mostly for compiling, running, testing, formatting and more. 

Configure your CI tool to also call the commands in the Makefile .
This way there is a standardized way of running the main tasks in your
project, and we place as little configuration as possible in the CI tool


## Pythonic Code 

In programming, an idiom is a particular way of writing code in order to
perform a specific task.

Careful : design patterns are high-level ideas, independent from the language (mostly), but they do not
translate into code immediately. On the other hand, idioms are actually coded. It is the way things should be written when we want to perform a particular task

writing code in an idiomatic way usually performs better. It is also more compact and easier to understand.

### Indexes/Slices & Sequence 

On suppose qu'on travaille avec la liste `L = [0,1,2,3,4,5,6,7,8]`

On peut accéder aux éléments de la liste avec `L[i]` ou `i`peut etre positif (sens direct) ou négatif (sens inverse)

On peut définir des slices de la liste avec `slice(i,j,k)` qui peut aussi être utilisé comme `L[i:j:k]` avec `k` le pas.

Cette façon d'indexer fonctionne grace à la magic method `__getitem__` i.e `L[i] == L.__getitem__(i)` (more on this later). 

On définit une séquence comme étant un objet qui implémente les méthodes `__getitem__` et `__len__`. 

### Context managers 

There are recurrent situations in which we want to run some code that has
preconditions and postconditions, meaning that we want to run things
before and after a certain main action. Context managers are perfect for this !

Context managers consist of two magic methods: `__enter__` and `__exit __`.
Imagine the class ContextManager implementing these two methods. 

On the first line of the context manager, the with statement will call the method `__enter__` and the return value of this function can be assigned to a new variable using `as`. 
After this line is executed, the code enters a new context where any other Python code can be run. 

After the last statement on that block is finished,
the context will be exited, meaning the magic method `__exit __` will be called. if there is an exception or error inside the context manager block, the `__exit__` method will still be called,

Signature : `__exit__(self, exc_type, ex_value, ex_traceback)`

There are other ways of implementing context managers, sometimes more compact, using the contextlib module. Writing context managers like this has the advantage that it is easier to
refactor existing functions, reuse code, and in general is a good idea when
we need a context manager that doesn't belong to any particular object

When the `contextlib.contextmanager` decorator is applied to a function, it
converts the code on that function (must be a generator) into a context manager.


```python
@contextlib.contextmanager
def db_handler():
    try: 
        stop_database()
        yield
    finally:
        start_database()
```

We can also write a context manager as a decorator and don't use `with` keyword. The decorator
doesn't know anything about the function that is decorating, and vice versa. They are completely independent this way. 
However if needed you can still call this decorator inside the function to access the object if you want to retrieve some objects. 

```python
class dbhandler_decorator(contextlib.ContextDecorator):
    def __enter__(self):
        stop_database()
        return self
    
    def __exit__(self, exc_type, ex_value, ex_traceback):
        start_database()

@dbhandler_decorator
def offline_backup():
    #Dump database
```

or 

```python
@dbhandler_decorator
def offline_backup():
    with dbhandler_decorator() as handler:
        #Dump database
```

A useful context manager to know is contextlib.supress

### Comprehensions and assignment expressions

...

### Properties, attributes, and different types of methods for objects

All of the properties and functions of an object are public in Python. 
An attribute that starts with an underscore is meant to be private to that object, and we expect that no external agent calls it (but again, nothing is preventing this).

`obj.__dict__` is equivalent to `vars(obj)`

#### Underscores conventions

1. `_var` or `_method`: should be accessed only within object itself and never from a caller. Having private methods and variables is useful so that you can safely refactor private things at all of the times if it's needed. 

Note : On the other hand, as an exception to this rule, we could say that in unit tests, it might be allowed to access internal attributes if this makes things easier to test ! 

Using too many internal methods and attributes could be a sign that the class has too many tasks and could indicate that you need to extract some of its responsibilities into more collaborating classes.

2. `__var` or  `__method` : There is a common misconception that some attributes and methods can be actually made private using leading double underscores. What's actually happening is that Python creates a different name for the attribute (mangling) : `_<class-name>__<attribute-name>`. Double leading underscores are a non-Pythonic approach.

#### Propreties

Typically when we do want to access some attributes of an object that are intended to be public we'd use properties.

Typically, in object-oriented design, we create objects to represent an abstraction over an entity of the domain problem. 
Some entities can only exist for certain values of the data, whereas incorrect values shouldn't be allowed.

Example for a Coordinate system 

```python
class Coordinate:
    def __init__(self, lat: float, long: float) -> None:
        self._latitude = self._longitude = None
        self.latitude = lat
        self.longitude = long

    @property
    def latitude(self) -> float:
        return self._latitude

    @latitude.setter
    def latitude(self, lat_value: float) -> None:
        if lat_value not in range(-90, 90 + 1):
            raise ValueError(f"{lat_value} is an invalid value")
        self._latitude = lat_value

    @property
    def longitude(self) -> float:
        return self._longitude

    @longitude.setter
    def longitude(self, long_value: float) -> None:
        if long_value not in range(-180, 180 + 1):
            raise ValueError(f"{long_value} is an invalid value")
        self._longitude = long_value
```

When any user wants to modify values for any of these properties `coordinate.latitude = <new-latitude-value>` it will automatically (and transparently) invok the setter method. 


In general, any time we ask an object about its current state, it should return it without side effects (without changing its internal representation). 
The only exception of to this rule would be in the case of a lazy property: something we want to precompute only once, and then use the computed value.


#### Classes with more compact syntax

We can simplify the initialization of classes by using the dataclasses.dataclass decorator, which, when applied to a
class, will take all the class attributes with annotations, and treat them as instance attributes. 

Additionally, this module provides a field object that will help us define particular traits for some of the attributes. 
Example : if one of the attributes needs to be mutable we shouldn't pass as a default parameter an empty mutable object
and we should instead pass None. 

```python
class Books:
    def __init__(self, pages : Optional[list] = None):
        self.pages = pages if pages else []
```

instead with dataclasses you will write 

```python
@dataclasses
class Books:
    pages = field(default_factory = list)
```


Questions : 

1. what happens if we need to run validations ? => Use `__post_init__` method that will be called automatically by `__init__`
2. what if we want to have some attributes computed or derived from previous ones ? => Use `properties`
3. When to prefer the usage of dataclass instead of regular class definition ? => When you have objects that don't do many complex validations or transformations on the data 

You can also have regular class attribute (not instantiable with new value) using  `size : int = field(init=False, default=R)` or `size = R` (without annotation)

Note : Annotations are great, but they don't enforce data conversion. If you declare an attribute that needs to be a float , or an integer , then you must do this conversion in the `__init__` method. it's perfectly fine in case of an object that can be created from multiple other types ! (this leverages Python's dynamic typing nature)

#### Iterables and sequences

We saw built-in iterables lists, tuples, sets, and dictionaries that we can iterate over with a for loop to get those values repeatedly.

We can create our own iterable , with the logic we define iteration relying on magic methods. Iteration works in Python by its own protocol (protocol). 

When you try to iterate an object using the keyword `for`, Python will search in order : 

1. If the object contains one of the iterator methods : `__next__` or `__iter__`
2. If the object is a sequence : has `__len__` and `__getitem__`

When we use the `for` keyword to iterate on an iterator object, python will call the `iter` magic method on it (calling `__iter__`). This way it will know that the object to iterate is a iterable. At every step of the loop it will call the `next` magic method (call `__next__`) until raising `StopIteration` exception. 


Example : 

```python
class DateRangeIterable:
    """An iterable that contains its own iterator object."""
    def __init__(self, start_date, end_date):
        self.start_date = start_date
        self.end_date = end_date
        self._present_day = start_date

    def __iter__(self):
        # self._present_day = start_date => Refill the iterator
        return self

    def __next__(self):
        if self._present_day >= self.end_date:
        raise StopIteration()
        today = self._present_day
        self._present_day += timedelta(days=1)
        return today
```

This code as a small issue. The iterable isn't refilled when we are done iterating it. One way to do it is in `__iter__` function to reassign the present_day as the start date. But a way more elegant and compact way to achieve the same result would be to use generators (are iterators object, more on this later), called a **container iterable**. 


```python
class DateRangeContainerIterable:
    def __init__(self, start_date, end_date):
        self.start_date = start_date
        self.end_date = end_date

    def __iter__(self):
        current_day = self.start_date
        while current_day < self.end_date:
            yield current_day
            current_day += timedelta(days=1)
```


If `__iter__` is not defined on the object, the `iter` function will look for the presence of `__getitem__` , and if this is not found, it will raise `TypeError`. A sequence is an object that implements `__len__` and `__getitem__` methods. 
Implementing a sequence will use more memory than a iterator because we have to hold everything at once ! 

Example : 

```python
class DateRangeSequence:
    def __init__(self, start_date, end_date):
        self.start_date = start_date
        self.end_date = end_date
        self._range = self._create_range()

    def _create_range(self):
        days = []
        current_day = self.start_date
        while current_day < self.end_date:
            days.append(current_day)
            current_day += timedelta(days=1)
        return days

    def __getitem__(self, day_no):
        return self._range[day_no]

    def __len__(self):
        return len(self._range)
```

Note : Evaluate the trade-off between memory and CPU usage when deciding which one of the two possible implementations to use. In general, the iteration is preferable (and generators even more), but keep in mind the requirements of every case.

#### Container objects

Containers are objects that implement a `__contains__` method and returns a Boolean. This method is called in the presence of the `in` keyword. 

`element in container` is equivalent to `container.__contains__(element)`. 

#### Dynamic attributes for objects

When we call something like `<myobject>.<myattribute>` , Python will look for `<myattribute>` in the dictionary of the object, calling `__getattribute__` on it. Equivalent to `<myobject>.__getattribute__("<myattribute>")`. 

If this is not found then the extra method, `__getattr__` , is called, passing the name of the attribute as a parameter.

If you want to access a attribute but not raise an error if it's not found you should use the function `getattr(obj, attribute_name, default_value)`. If `default_value` is not set, it will raise the `AttributeError` exception. 

Note : Be careful when implementing a method so dynamic as `__getattr__`, and use it with caution. Don't forget when implementing `__getattr__` to raise AttributeError. 

In which case should we use dynamic attributes ?

1. To create a proxy to another object 

```python
@dataclass
class A:
    foo : int = 42

@dataclass
class B:
    a = A()
    def __getattr__(self, attr):
        return getattr(self.a, attr)

b = B()
assert b.foo == 42
```

2. When you know you need attributes that are dynamically computed. Example : When property X is requested use resolve_X method to compute new value and send it to the user (mmhhh not clear should ask chatgpt to explain more, but it is for anothe day !!!)



```python
class GrapheneObject:
    def __init__(self, domain_object):
        self.domain_object = domain_object

    def __getattr__(self, name):
        # Check if a resolve_X method exists for the requested property
        method_name = f"resolve_{name}"
        if hasattr(self, method_name) and callable(getattr(self, method_name)):
            # If the method exists, call it to compute the property value
            return getattr(self, method_name)()
        else:
            raise AttributeError(f"'GrapheneObject' object has no attribute '{name}'")

    def resolve_name(self):
        # Example: Resolve the 'name' property based on the domain_object
        return self.domain_object.get_name()

    def resolve_age(self):
        # Example: Resolve the 'age' property based on the domain_object
        return self.domain_object.get_age()


# Example domain object
class DomainObject:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def get_name(self):
        return self.name

    def get_age(self):
        return self.age


# Usage
domain_obj = DomainObject(name="Alice", age=30)
graphene_obj = GrapheneObject(domain_obj)

# Access properties using the naming convention
print(graphene_obj.name)  # Calls resolve_name to get the 'name' property
print(graphene_obj.age)   # Calls resolve_age to get the 'age' property

# Accessing a property that doesn't have a corresponding resolve_X method will raise an AttributeError
# print(graphene_obj.address)  # This will raise an AttributeError

```

#### Callable objects

The magic method `__call__` will be called when we try to execute our object as if it were a regular function.

The main advantage of implementing functions this way, through objects, is that objects have states, so we can save and maintain information across calls.

This method comes in handy when creating decorators.


#### Summary


| Statement | Magic method | Behavior |
| ----------- | ----------- | ----------- |
| `obj[i] / obj[i:j] / obj[i:j:k]` | `__getitem__(key)` | Subscriptable object | 
| `with obj: ... `| `__enter__ / __exit__`| Context manager |
| `for i in obj: ...` | `__iter__ / __next__` | Iterable object |
| `for i in obj: ...` | `__len__ / __getitem__` | Sequence |
| `obj in container` | `__contains__` | Container |
| `obj.<attribute>` | `__getattr__` | Dynamic attribute retrieval |
| `obj(*args,**kwargs)` | `__call__(*args, **kwargs)` | Callable object |

The best way to implement these methods correctly is to declare our class to
implement the corresponding class following the abstract base classes
defined in the `collections.abc` module. These interfaces
provide the methods that need to be implemented and will create the type correctly.

### Caveats in Python

* Mutable default arguments : NEVER USE THEM AS DEFAULT ARGUMENTS because of the scope. 
* Extending built-in types : The correct way of extending built-in types such as lists, strings, and dictionaries is by means of the collections module. 

### Asynchronous programming 


## General traits of code programming 

### Inheritance in Python

Inheritance is a powerful concept but it does come with its perils. The
main one is that every time we extend a base class, we are creating a new
one that is tightly coupled with the parent. Remember good code should have high cohesion and low coupling. 

One of the main scenarios developers relate inheritance with is code reuse but at the same time it is not a good idea to force
our design to use inheritance to reuse code just because we get the methods from the parent class for free.

The proper way to reuse code is to have highly cohesive objects that can be easily composed and that could work on multiple contexts.

#### When inheritance is a good decision ?

Take off : The correct use of inheritance is to specialize objects and
create more detailed abstractions starting from base ones.

Inheritance is a double-edged sword : we get all the code of the methods from the parent class for free, but on the other hand, we are carrying all of them to a new class, meaning that we might be placing too much functionality in a new definition.

As a heuristic to see whether a class is correctly defined using inheritance we have to think if it is actually going to use
all of the methods it has just inherited. If the answer is no then this is a design mistake that could be caused by a number of reasons : 

* The superclass (class from which we inherit from) is vaguely defined and contains too much responsibility, instead of a well-defined interface
* The subclass is not a proper specialization of the superclass it is trying to extend

A good case for using inheritance is the type of situation when you have a
class that defines certain components with its behavior that are defined by
the interface of this class. Then you
need to specialize this class in order to create objects that do the same but with something else added, or with some particular parts of its behavior
changed.

When we want to enforce the interface of some objects, we can create an
abstract base class that does not implement the behavior itself, but instead
just defines the interface. 


#### Multiple inheritance in Python

As inheritance, when improperly used, leads to design problems, you could also expect that multiple inheritance will also yield even bigger problems when it's not correctly implemented.

One of the most powerful applications of multiple inheritance is perhaps that which enables the creation of mixins but we need
first to understand how multiple inheritance works, and how methods are resolved in a complex hierarchy.

##### Method Resolution Order : MRO

Multiple inheritance is not much liked or used in other programming languages because of some constraints on them. One example is the so-called diamond problem.

When a class extends from two or more classes, and all of those classes also extend from other base classes, the bottom ones will have multiple ways to resolve the methods coming from the top-level classes.
The question is: Which of these implementations is used ?

Python resolves this by using an algorithm called C3 linearization or MRO, which defines a deterministic way in which methods are going to be called. [wikipedia](https://en.wikipedia.org/wiki/C3_linearization)

We can specifically ask the class for its resolution order using the `.mro()` method. 

##### Mixins

A mixin is a base class that encapsulates some common behavior with the goal of reusing code.
Typically, a mixin class is not useful on its own (you shouldn't be able to instanciate an object for it), and extending this class alone will certainly not work, because most of the time it depends on methods and properties that are defined in other classes.



#### Anti-patterns for inheritance

we will see a typical
situation that represents a common anti-pattern in Python. There is a
domain problem that has to be represented, and a suitable data structure is
devised for that problem, but instead of creating an object that uses such a
data structure, the object becomes the data structure itself.

First, we will define the problem we aim to solve. 
we
have a system for managing insurance, with a module in charge of applying
policies to different clients. We need to keep in memory a set of customers
that are being processed at the time in order to apply those changes before
further processing or persistence. The basic operations we need are to store
a new customer with its records as satellite data, apply a change to a policy,
or edit some of the data, to name but a few. We also need to support a batch
operation. That is, when something on the policy itself changes (the one this
module is currently processing), we have to apply these changes overall to
customers on the current transaction.

Then we should use or define a suitable data structure for this problem, we realize that accessing
the record for a particular customer in constant time is a nice trait.
It's generally here that we start adding complexity and start using anti pattern of inheritance. 

For example something like `policy_transaction[customer_id]` looks like a nice interface.
We might think that a subscriptable object is a good idea, and further on, we might get carried away into thinking that the object we need is a dictionary. 

```python
class TransactionalPolicy(collections.UserDict):
    """Example of an incorrect use of inheritance."""
    def change_in_policy(self, customer_id, **new_policy_data):
        self[customer_id].update(**new_policy_data)
```

Here we have the interface we want but at what cost ? If we check all the methods of this class we will find a lot of them related to the structure of dictionnary and that are of no use to us in this case !

There are (at least) two major problems with this design : 


1. Hierarchy is wrong : ceating a new class from a base one conceptually means that it's a more specific version of the class it's extending however that's not the case here (How is it that a TransactionalPolicy is a dictionary?) 
2. High coupling : The interface of the transactional policy now includes all unecessary methods from a dictionary. 

Take off : This is a problem of mixing implementation objects with domain objects. A dictionary is an implementation object, a data structure, suitable for certain kinds of operation, and with a trade-off like all data structures. A transactional policy should represent something in the domain problem, an entity that is part of the problem we are trying to solve.

The correct solution here would be to use composition of an existing data structure and to redefine the interface of a sequence for our business object. 

```python
class TransactionalPolicy:
    """Example refactored to use composition."""
    def __init__(self, policy_data, **extra_data):
        self._data = {**policy_data, **extra_data}
    def change_in_policy(self, customer_id, **new_policy_data):
        self._data[customer_id].update(**new_policy_data)
    def __getitem__(self, customer_id):
        return self._data[customer_id]
    def __len__(self):
        return len(self._data)
```

This way the code is conceptually correct and also more extensible. For example if we decide in the future that the underlying data structure (here a dictionnary) should change to another type (schema, list, whatever) callers of this object will not be affected, so long as the interface is maintained. This reduces coupling, minimizes ripple effects, allows for better refactoring (unit tests ought not to be changed), and makes the code more maintainable.

### Arguments in functions and methods


In Python, functions can be defined to receive arguments in several different ways. There is a set of practices for defining interfaces in software engineering that closely relate to the definition of arguments in functions. 

#### How functions works in Python ? 

##### Arguments passed by a value 

The first rule in Python is that all arguments are passed by a value (contrary to passed by reference). We can pass them by position and also by keyword. 

A function can mutate or not mutate the arguments that it receives depending on the type. If we pass mutable arguments and the function modifies it then we have the side effect that it will have been changed by the time the function returns. 

We can see an exemple with 2 objects that are passed to a function and analyze the behavior of the side effects. 


```python
def func(arg):
    arg += ' in function'
    print(arg)

immutable_obj = 'hello'
func(immutable_obj)
assert immutable_obj == 'hello'

mutable_obj = list('hello')
func(mutable_obj)
assert mutable_obj == list('hello in function')
```

This might look like an inconsistency, but it's not. When we pass the first argument, a string , this is assigned to the argument on the function. 

For immutable objects doing `argument += <expression>` will create a new object `argument + <expression>` and assign it back to the `argument`. In the scope of the function the argument value is now equal to `'hello in function'` and has nothing to do with the original one in the caller.

On the other hand, when we pass list , which is a mutable object, then the statement `argument += <expression>`has a different meaning. This operator call behind the scene the method `.extend()`. In fact we passed by value the list reference (pointer) but since it's a reference, it is mutating the original list object because of how the extend methods works. 

We have to be careful when dealing with mutable arguments because it can lead to unexpected side effects, we should try to avoid unnecessary side effects in functions as much as possible. 


##### Variable number of arguments

We will cover the basic principles of functions with a variable number of arguments, along with some recommendations.

For a variable number of positional arguments, the star symbol ( * ) is used, preceding the name of the variable that is packing those arguments. This works through the packing mechanism of Python.


###### Packing mechanism 

We can unpack and pack variables. 

```python
def func(first, second, third):
    return first, second, third

l = [1,2,3]
assert func(l[0], l[1], l[2]) == func(*l) #More cleaner to read
```

Here we packed the arguments and passed them all together. 
It also works the other way around. We could unpack the value of a list. 

```python
first, *rest = range(6)
assert (first, rest) == (0, [1,2,3,4,5])

*rest, last = range(6)
assert (rest, last) == ([0,1,2,3,4], 5)

first, *middle, last = range(6)
assert (first, middle, last) == (0, [1,2,3,4], 5)

first, last, *empty = 1, 2
assert (first, last, empty) == (1,2,[])
```

One of the best uses for unpacking variables can be found in iteration
When we have to iterate over a sequence of elements, and each element is, in turn, a sequence, it is a good idea to unpack at the same time each element is being iterated over.

Example of the max function of the stdl 

```
max(...)
    max(iterable, *[, default=obj, key=func]) -> value
    max(arg1, arg2, *args, *[, key=func]) -> value
```

There is here a similar notation, with two stars ( ** ) for keyword arguments. If we have a dictionary and we pass it with a double star to a function, what it will do is pick the keys as the name for the parameter, and pass the value for that key as the value for that parameter in that function. The same packing and unpacking applies to keyword arguments. 


This feature lets us choose dynamically the values we want to pass to a function. However, abusing this functionality, and making excessive use of it, will render the code harder to understand. A good recommendation is to not use this dictionary to extract particular values from it but to extract these arguments directly on the function definition.

##### Positional-only parameters

If we don't make use any special syntax when defining the function arguments, by default, they can be passed by position or keyword.
```python
def func(x,y):
    return x+2*y

assert func(1,2) == func(x=1,y=2) == func(y=2, x=1) == func(1,y=2) == func(**{'x':1, 'y':2})
```

The only constraint here is that if we pass one argument as a keyword, all the following ones must be provided as a keyword as well. 
For example `func(y=2, 1)` wouldn't work. 

Starting from Python 3.8 (PEP-570), new syntax was introduced that allows parameters to be defined that are strictly positional. 

```python
def func(x,y, /):
    return x+2*y
```

Here `func(x=1, y=2)` would be incorrect and only `func(1,2)` would be correct. 
The error would be 
```python
TypeError: func() got some positional-only arguments passed as keyword arguments: 'x, y'
```

In which case will this behaviour be useful ? there could be situations in which this syntax is useful, for example, in cases where the names of the arguments aren't meaningful (because of the context not of poor naming !) and attempting to use their name would be counterproductive. For the rest of the cases, this should be avoided.

##### Keyword-only arguments

In this case we use the * symbol to signal when the keyword-only arguments start. In the function signature, everything that comes after the variable number of positional arguments (*args) will be keyword-only.

This functionality is useful for extending functions or classes that are already defined (and being used) in a backward-compatible fashion.

#### The number of arguments in functions

In this section, we agree on the idea that having functions or methods that take too many arguments is a sign of bad design and we will propose ways of dealing with this issue.

It's quite simple there are two main solutions. 

1. Reification: Compacting multiple arguments into a new object (abstraction was missing)
2. Dynamic arguments : While using args and kwargs might be a Pythonic way of proceeding, we have to be careful not to abuse the feature, because we might be creating something that is so dynamic that it is hard to maintain.

##### Function arguments and coupling

The more arguments a function signature has, the more likely this one is going to be tightly coupled with the caller function.

If the called function takes a lot of parameters from the caller function we can derive some conclusions on the coupling 

1. The called function is a leaky abstraction and the caller function may be able to do it by itself. 
2. it is hard to imagine using the called function in a different context, making it harder to reuse.

##### Refactor compact function signatures that take too many arguments

Suppose we find a function that requires too many parameters. We know that we cannot leave the code base like that, and a refactor process is imperative. But what are the options?

Sometimes, there is an easy way to change parameters if we can see that most of them belong to a common object.     

```python
track_request(request.headers, request.ip_addr, request.parameters)
```

Here we could just pass directely as argument the `request`, it will simplify the signature and is more correct semanticaly. 

Note : While passing around parameters like this is encouraged, in all cases where we pass mutable objects to functions, we must be really careful at every time about side effects ! The function we are calling should not make any modifications to the object we are passing and a better alternative would be to copy it and return a (new) modified version of it if we plan to make modification on it.


If the previous strategies don't work (because the grouping of arguments in one object doesn't make sense), as a last resort we can change the signature of the function to accept a variable number of arguments. If the number of arguments is too big, using *args or **kwargs will make things harder to follow, so we have to make sure that the interface is properly documented and correctly used, but in some cases, this is worth doing.


## Improve code using decorators 


### What are decorators ? 

Decorators were introduced as a mechanism to simplify the way functions and methods are defined when they have to be modified after their original definition. The syntax for decorators improves the readability significantly.

In Python, functions are regular objects just like pretty much anything else. That means you can assign them to variables, pass them around by parameters, or even apply other functions to them. An important use case of decorators are for composing functions. 


Exemple : 

```python
def original(...):
    ...
original = modifier(original)
```

is equivalent to 

```python
@modifier
def original(...):
    ...
```

Here `modifier` is what we call the **decorator**, and `original` is the decorated function, often also
called a **wrapped** object.

Note : While the functionality was originally thought of for methods and functions, the actual syntax allows any kind of object to be decorated, so we are going to explore decorators applied to functions, methods, generators, and classes.



#### Function decorators

We can use decorators on functions to apply all sorts of logic to them : we can validate parameters, check preconditions, change the behavior entirely, modify its signature, cache results (create a memoized version of the original function), and more.



Example of a simple retry decorator : 


```python
def retry(operation):
    @wraps(operation) #The use of @wraps can be ignored for now
    def wrapped(*args, **kwargs):
        last_raised = None
        RETRIES_LIMIT = 3
        for _ in range(RETRIES_LIMIT):
            try:
                return operation(*args, **kwargs)
            except CustomError as error: #Choose an Exception to catch
                logger.warns("retrying %s", operation.__qualname__)
                last_raised = error
        raise last_raised
    return wrapped
```


This decorator doesn't take any parameter and could be applied to any function or method using the following syntax : 

```python
@retry
def run_operation(task):
    return task.run()
```

This way of composing function is really great for code readability and code reuse. 



#### Classes decorators


Classes are also objects in Python, that means the same considerations to functions apply; they can also be passed by parameters, assigned to variables, asked some methods, or be transformed. The only difference is that when writing the code for this kind of decorator, we have to take into consideration that we are receiving a class as a parameter of the wrapped method, not another function.


To underestand classes decorators we will work again with the monitoring system example. Recalling our event systems for the monitoring platform, we now need to transform the data for each event and send it to an external system.
However, each type of event might have its own particularities when selecting how to send its data. In particular, the event for a login might contain sensitive information such as credentials that we want to hide (serialize). 

A first solution could be having a class that maps to each particular event and knows how to serialize it. 


```python
class LoginEventSerializer:
    def __init__(self, event):
        self.event = event
    def serialize(self) -> dict:
        return {
        "username": self.event.username,
        "password": "**redacted**",
        "ip": self.event.ip,
        "timestamp": self.event.timestamp.strftime("%Y-%m-%d-%H:%M"),
        }
@dataclass
class LoginEvent:
    SERIALIZER = LoginEventSerializer
    username: str
    password: str
    ip: str
    timestamp: datetime
    def serialize(self) -> dict:
        return self.SERIALIZER(self).serialize()
```

This design is pretty bad when we want to extend our system

1. Too many classes : As the number of events grows, the number of serialization classes will grow (one-to-one mapping)
2. Solution is not flexible enough : if we need to reuse parts of the components we will have to extract this into a function
3. Boilerplate: Each Even shoud have a serialize method. 


A better solution could be to dynamically construct an object that, given a set of transformation functions and an event instance, can serialize it by applying the filters to its fields.


```python
from dataclasses import dataclass

### Transformation function
def hide_field(field) -> str:
    return "**redacted**"
def format_time(field_timestamp: datetime) -> str:
    return field_timestamp.strftime("%Y-%m-%d %H:%M")
def show_original(event_field):
    return event_field

### Serializer 


class EventSerializer:
    def __init__(self, serialization_fields: dict) -> None:
        self.serialization_fields = serialization_fields
    def serialize(self, event) -> dict:
        return {
            field: transformation(getattr(event, field))
            for field, transformation
            in self.serialization_fields.items()
        }

class Serialization:
    def __init__(self, **transformations):
        self.serializer = EventSerializer(transformations)
    def __call__(self, event_class):
        def serialize_method(event_instance):
            return self.serializer.serialize(event_instance)
        event_class.serialize = serialize_method # Add dynamically to the event_class the serialize method 
        return event_class


### Events 

@Serialization(
    username=str.lower,
    password=hide_field,
    ip=show_original,
    timestamp=format_time,
)
@dataclass #Chaining of decorators, first into a data class then serialize fields => Serialization(dataclass(LoginEvent()))
class LoginEvent:
    username: str
    password: str
    ip: str
    timestamp: datetime
```

Now, the code of the class does not need the serialize() method defined, nor does it need to extend from a mixin that implements it, since the decorator will add it.


Potential drawbacks of extensive use of classes decorators : might jeopardize readability because we would be declaring some attributes and methods in the class, but behind the scenes, the decorator might be applying changes that would render a completely different class.


### Advanced decorators

Now we're interested in more advanced uses of decorators that will help us structure our code more cleanly. We'll see that we can use decorators to separate concerns into smaller functions, and reuse code. So we need a way to pass parameters to them (like for the class decorators). 


#### Passing arguments to decorators

Decorators could be even more powerful if we could just pass parameters to them so that their logic is abstracted even more. There are several ways of implementing decorators that can take arguments. 

1. Create decorators as nested functions with a new level of indirection => everything in the decorator fall one level deeper.
2. Use a class decorator => implement a callable object that still acts as a decorator


Note : the second approach favors readability more, because it is easier to think in terms of an object than nested functions 

##### Decorators with nested functions
The general idea of a decorator is to create a function that returns another function. The internal function defined in the body of the decorator is going to be the one being called and that's the one we want to pass parameter to. 

If we wish to pass parameters to it, we then need another level of indirection. The first function will take the parameters, and inside that
function, we will define a new one, which will be the decorator, which in turn will define yet another new function, namely the one to be returned as a result of the decoration process.


We will take again the example of the retry decorator where we could define the number of max retries as an argument (we could even add a default value to this parameter). So we want to define an object with_retry(arg1, arg2,... ) that will be a decorator and can be used like this with_retry(arg1, arg2, ...)(function) that could translate in pythonic 

```python
@with_retry(arg1, arg2, ...)
def run_operation(task):
    task.run()
```

Ok let's write the corresponding decorator



```python
_DEFAULT_RETRIES_LIMIT = 3
def with_retry(
    retries_limit: int = _DEFAULT_RETRIES_LIMIT,
    allowed_exceptions: Optional[Sequence[Exception]] = None        
):
    allowed_exceptions = allowed_exceptions or CustomException
    def retry(operation):### Then we just copy the previous retry decorator
        @wraps(operation) #The use of @wraps can be ignored for now
        def wrapped(*args, **kwargs):
            last_raised = None
            for _ in range(retries_limit):
                try:
                    return operation(*args, **kwargs)
                except allowed_exceptions: as error: #Choose an Exception to catch
                    logger.warns("retrying %s", operation.__qualname__, error)
                    last_raised = error
            raise last_raised
        return wrapped
    return retry
```

Basically that's it, not to much complicated and we have a reusable decorator for specific cases


```python
@with_retry() #Will use default value, @with_retry will not work. 
def run_operation(task): 
    return task.run()
@with_retry(retries_limit=5)
def run_with_custom_retries_limit(task):
    return task.run()
@with_retry(allowed_exceptions=(AttributeError,))
def run_with_custom_exceptions(task):
    return task.run()
```


Nested functions to implement decorators are probably the first thing we'd think of and this works well for most cases. However as you might have noticed, the indentation keeps adding up, for every new function we create, so soon it might lead to too many nested functions. 

Also, functions are stateless, so decorators written in this way won't necessarily hold internal data, as objects can. (example for caching)


##### Decorator objects


A cleaner implementation of this would be to use a class to define the decorator. In this case, we can pass the parameters in the `__init__` method, and then implement the logic of the decorator on the magic method named `__call__`.


Here the same code implemented using class decorator 


```python
_DEFAULT_RETRIES_LIMIT = 3

class WithRetry:
    def __init__(
        self,
        retries_limit: int = _DEFAULT_RETRIES_LIMIT, 
        allowed_exceptions: Optional[Sequence[Exception]] = None  
    ):
        self.retries_limit = retries_limit
        self.allowed_exceptions = allowed_exceptions
    
    def __call__(self, operation):
        @wraps(operation) #The use of @wraps can be ignored for now
        def wrapped(*args, **kwargs):
            last_raised = None
            for _ in range(self.retries_limit):
                try:
                    return operation(*args, **kwargs)
                except self.allowed_exceptions: as error: #Choose an Exception to catch
                    logger.warns("retrying %s", operation.__qualname__, error)
                    last_raised = error
            raise last_raised
        return wrapped
```

And it can be used as the nested decorator 

```python
@WithRetry() #Will use default value, @with_retry will not work. 
def run_operation(task): 
    return task.run()
@WithRetry(retries_limit=5)
def run_with_custom_retries_limit(task):
    return task.run()
@WithRetry(allowed_exceptions=(AttributeError,))
def run_with_custom_exceptions(task):
    return task.run()
```


#### Decorators for coroutines


:TODO:




### Good uses for decorators