# Philosophy of good code

We will start by studing general traits of good programming then a list of applicative principles that can be used in our code design. 

The ultimate goal is to make the code as robust as possible, and to write it in a way that minimizes defects or makes them utterly evident, should they occur. High-quality code is a concept that has multiple dimensions and is difficult to achieve. 

We will study this philosphy applied to the programming language Python. 


## General Traits of Good Code

Good software is built from a good design and the code *is* the design. The code is probably the most detailed representation of the design. We will study and focus on design principles at a higher level of abstraction.

Good quality software should be built around certains principles, and they will serve as design tools. However that does not mean that all of them should always be applied. They will help us to design maintainable software that can easily be reused, extended and adapted to new requirements. 

These concepts are fundamental design ideas that lay the foundations for more advanced topics, such as SOLID principles.

### Desing By Contrat

#### Context

Some parts of the software we are working on are not meant to be called directly by users, but instead by other parts of the code. We have to encapsulate some functionality behind each component and expose an interface to clients who are going to use that functionality, namely, an Application Programming Interface (API).

The functions, classes, or methods we write for that component have a particular way of working under certain considerations that, if they are not met, will make our code crash. Conversely, clients calling that code expect a particular response, and any failure of our function to provide this would represent a defect.


Of course, when designing an API, the expected input, output, and side effects should be documented. 
But **documentation cannot enforce the behavior of the software at runtime.** 
These rules, what every part of the code expects in order to work properly and what the caller is expecting from them, should be part of the design, and here is where the concept of a **contract** comes into place.

The idea behind the DbC approach is that both parties agree on a contract that, if violated, will raise an exception, clearly stating why it cannot continue.


#### Contract definition

A contract is a construction that enforces some rules that must be honored during the communication of software components.
A contract entails mainly the following rules : 

* Preconditions : Validation performed before letting the code run. This imposes a constraint on the caller. 
* Postconditions : the validations are done after the function call is returned. validate what the caller is expecting from this component.
* [Optional] Invariants : document, in the docstring of a function, the things that are kept constant while the code of the function is running, as an expression of the logic of the function to be correct.
* [Optional] Side effects : document, in the docstring of a function, any side effects

Only the first two are to be enforced at the code level (low level)


The main reason of using Design by Contract is that if errors occur, they must be easy to spot and to find the culprit more easily so that they can be quickly corrected. More importantly, we want critical parts of the code to avoid being executed under the wrong assumptions.

Preconditions bind the client (they have an obligation to meet them if they want to run some part of the code), whereas postconditions bind the component in relation to some guarantees that the client can verify and enforce.
If the precondition fails, we know it is due to a defect on the client. On the other hand, if the postcondition check fails, we know the problem is in the routine or class (supplier) itself.

##### Preconditions

In general programming terms, this means providing data that is properly formed. For Python, in particular, being dynamically typed, this also means that sometimes we need to check for the exact type of data that is provided (different of type checking)

The question is where to place the validation logic ? 

1. Tolerant approach : let the clients validate all the data before calling the function (function itself will allow any data)
2. Demanding approach : let the function validate all the data prior to running its own logic. 


Regardless of the approach we decide to take, we should always keep in mind the non-redundancy principle, which states that the enforcement of each precondition for a function should be done by only one of the two parts of the contract, but not both.

Note : Demanding approach is usually the safest choice in terms of robustness, and usually the most common practice in the industry.

##### Postconditions

Postconditions are the part of the contract that is responsible for enforcing the state after the method or function has returned assuming that the preconditions were met. 

The idea is to use postconditions to check and validate everything that a client might need. If the method executed properly, and the postcondition validations pass, then any client calling that code should be able to work with the returned object without problems, as the contract has been fulfilled.



#### Conclusion 

The main value of this design principle is to effectively identify where the problem is. As a result of following this principle, the code will be more robust. Each component is enforcing its own constraints and maintaining some invariants, and the program can be proven correct as long as these invariants are preserved.

Of course, following these principles also adds extra work, because we are
not just programming the core logic of our main application, but also the
contracts. In addition, we might want to consider adding unit tests for these
contracts as well. However, the quality gained by this approach pays off in
the long run.

Nonetheless, for this method to be effective, we should carefully think about what we are willing to validate, and this has to be a meaningful value. It doesn't make much sense to define contracts that only check for the correct data types of the parameters provided to a function, that would be like trying to make Python a statically typed language. 

We would also like to keep the code as isolated as possible. That is, the code for the preconditions in one part, the one for the postconditions in another, and the core of the function separated. We could achieve this separation by creating smaller functions, but in some cases implementing a decorator would be an interesting alternative.

### Defensive Design Programming 

Defensive programming follows a somewhat different approach to DbC. This is more about making all parts of the code (objects, functions, or methods) able to protect themselves against invalid inputs.

Defensive programming is a technique that has several aspects, and it is particularly useful if it is combined with other design principle like DbC. 

The main ideas on the subject of defensive programming are how to handle errors for scenarios that we might expect to occur (error handling procedures) and how to deal with errors that should never occur (case for assertions). 


#### Error handling 

The idea behind error handling is to gracefully respond to these expected
errors in an attempt to either continue our program execution or decide to
fail if the error turns out to be insurmountable.

There are different approaches by which we can handle errors on our programs, but not all of them are always applicable.

##### Value substitution

In some scenarios, when there is an error and there is a risk of the software producing an incorrect value or failing entirely, we might be able to replace the result with another, safer value. We are replacing the actual erroneous result for a value that is to be considered non-disruptive. 

This strategy has to be carefully chosen for cases where the substituted value is a safe option.
Making this decision is a trade-off between robustness and correctness. A software program is robust when it does not fail, even in the presence of an erroneous scenario. But this is not correct either.

This might not be acceptable for some kinds of software. If the application is critical, or the data being handled is too sensitive, this is not an option, since we cannot afford to provide users (or other parts of the application) with erroneous results.

A slightly different, and safer, version of this decision is to use default
values for data that is not provided. This can be the case for parts of the
code that can work with a default behavior, for example, default values for
environment variables that are not set, for missing entries in configuration
files, or for parameters of functions.

In general, replacing missing parameters with default values is acceptable, but substituting erroneous data with legal close values is more dangerous and can mask some errors. Take this criterion into consideration when deciding on this approach.


##### Exception handling

In the presence of incorrect or missing input data, sometimes it is possible to correct the situation using value substitution. In other cases, however, it is better to stop the program from continuing to run with the wrong data than to leave it computing under erroneous assumptions. Nonetheless, erroneous input data is not the only possible way in which a function can go wrong. After all, functions are not just about passing data around; they also have side effects and connect to external components.

In those cases, failing and notifying the caller that something is wrong is a good approach. The function should clearly, and unambiguously, notify the rest of the application regarding errors that cannot be ignored so that they can be addressed accordingly. 

The mechanism for accomplishing this is an exception.

Important note : Do not use exceptions as a go-to mechanism for business logic. Raise exceptions when there is something wrong with the code that callers need to be aware of.

Exceptions are usually about notifying the caller about something amiss. This means that exceptions should be used carefully because they weaken encapsulation.

The more exceptions a function has, the more the caller function will have to anticipate, therefore knowing about the function it is calling. And if a function raises too many exceptions, this means that it is not so context-free, because every time we want to invoke it, we will have to keep all of its possible side effects in mind.

Note : This can be used as a heuristic to tell when a function is not sufficiently cohesive and has too many responsibilities. If it raises too many exceptions, it could be a sign that it has to be broken down into multiple, smaller ones.

Recommandations for using exceptions in Python : 

1. Handling exceptions at the right level of abstraction : the separation of the exception classes also delimits a separation in responsibilities.

2. Do not expose tracebacks to end users : This is a security consideration. In Python, tracebacks of exceptions contain very rich and useful debugging information. Unfortunately, this information is also very useful for attackers or malicious users who want to try and harm the application, not to mention that the leak would represent an important information disclosure, jeopardizing the intellectual property of your organization 

3. Avoid empty except blocks: This was even referred to as the most diabolical Python anti-pattern. You should catch a more specific exception and perform some actual error handling on the except block. If you want to really ignore a Exception you should use the contextmanager `contextlib.suppress` which can accept all exceptions as arguments that are to be ignored. 

4. Include the original exception : As part of our error handling logic, we might decide to raise a different one, and maybe even change its message. If that is the case, it is recommended to include the original exception that led to that. We can use the `raise <e> from <original_exception>` syntax. 
When using this syntax, the original traceback will be embedded into the new exception, and the original exception will be set in the `__cause__` attribute of the resulting one.

#### Using assertions in Python

Assertions are to be used for situations that should never happen, so the expression on the `assert` statement has to mean an impossible condition.

The idea of using assertions is to prevent the program from causing further damage if such an invalid scenario is presented. Sometimes, it is better to stop and let the program crash rather than let it continue processing under the wrong assumptions.

Analogie avec le corps humain, quand on tombe legerement malade notre corps nous alerte d'un probleme et peu meme interrompre une partie de l'execution (raise) si la zone atteinte n'est pas trop importante (ex : bras cassé ne peut plus bougé mais les autres fonctions sont dispo). Assert lui se comporte comme un shutdown du systeme interne (coma) pour ne pas affecter les fonctions vitales qui pourraient amener dans un état plus grave (la mort).

Make sure that the program terminates when an assertion fails and include a descriptive error message in the assertion statement and log the errors to make sure that you can properly debug and correct the problem later on. Don't forget the point of assertion that is to precisely let us know about those parts of the program that need to be fixed.

Note : `assert` statements are removed when the compilation is optimized, whereas `raise` is not. In particular, running with the –O flag will suppress the assert statements, but this is discouraged for the aforementioned reasons.


You could ask why do we need to use assertions when we can do everything with raise statement ? In
general, exceptions are for handling unexpected situations in relation to the business logic that our program will want to consider, whereas assertions are like self-checking mechanisms put in the code, to validate (assert) its correctness.

Typical uses of assert are situations where an algorithm maintains an invariant logic that must be kept at all times: in that case, you might want to assert for the invariant. If this is broken at some point, it means either the algorithm is wrong or poorly implemented.

### Separation of concerns

It's a design principle that is applied at multilevel (low and high level, code and architecture). The principle is simple, it states that different responsabilities should go into different components. Each part should be responsible for a functionnality without knowing about the other parts. 

The goal of this principle is to : 
* enhance maintainability : no need to know what part does what when working on the code
* minimize ripple effects : minimize the propagation of a change from a starting point

Example of bad separation of concerns : 

1. Error triggering a chain of other exceptions, causing failures that will result in a defect on a remote part of the application.
2. Having to change a lot of code scattered through multiple parts of the code base, as a result of a simple change in a function definition.

To discuss seperation of concerns we need 2 concepts to define our degree of separations : 

1. Cohesion : Components should have a small, well defined purpose and should do as little as possible. More cohesive implies more useful and reusable. 

2. Coupling : How two or more component depend on each other. If two part of the code are too coupled we can have no possibility to reuse the code, ripple effects and low level of abstraction. 

When thinking about the architecture or the code we should aim for high cohesion and low coupling. 


### Acronyms

#### Don't Repeat Yourself : DRY

Things in the code, knowledge, have to be defined only once and in a single place. When you have to make a change to the code, there should be only one rightful location to modify. Failure to do so is a sign of a poorly designed system.

Code duplication is a problem that directly impacts maintainability : 

* Error prone : we depend on efficiently correcting all the instances with this logic, without forgetting any of them
* Expensive : making a change in multiple places takes much more time than if it was defined only once.

Unfortunately, there is no general rule or pattern to tell you which of the features of Python are the most suitable when it comes to addressing code duplication. Depending on the case, the best solution would be different. In some cases, there might be an entirely new object that has to be created (maybe an entire abstraction was missing). In other cases, we can eliminate duplication with a context manager. Iterators, generators and decorators could also help to avoid repetition. 

Never forget that code represents knowledge and should be defined most of the time only once !

But what if we're trying to avoid duplication of a small section, let's say three lines of code ?
In that case, writing the decorator
would probably take more lines and be more trouble for the simple
duplicated lines we're trying to solve. In this case, apply common sense and
be pragmatic. Accept that a small amount of duplication might be better
than a complicated function (that is, of course, unless you find an even
simpler way of removing the duplication and keeping the code simple!).

#### You Ain't Gonna Need It : YAGNI

Generally when developping an application we want to be able to easily modify our programs, so we want to make
them future-proof. Some developers took this road and started doing futurology, they try to anticipate all future requirements and create solutions that are very complex, and so create abstractions that are hard to read, maintain, and understand. Now it is even harder to refactor and extend our programs and it's a HUGE mistake that's quite hard to correct. 

Remember, maintainable software is about writing software that only addresses current requirements in such a way that it will be possible (and easy) to change later on.

It's usually tempting to not follow this idea in some cases but we should not do it. For exemple we must refuse the temptation to apply design patterns prematurely as it might fall into a violation of the YAGNI principle. 


We can study a simple example. We're creating a class to encapsulate the behavior of a component (DRY principle). You know it's needed (current requirement) but then you think that more (and similar) requirements will come in the future, so it might be tempting to create a base class (as to define an interface with the methods that must be implemented), and then make the class you were just creating a subclass that implement that interface.

This example is wrong for multiple reason : 
1. all you need now is the class that was being created in the first place. investing more time in over-generalizing a solution that we don't know we'll need is over-engineering
2. base class is being biased by the current requirements, so it'll likely not be the correct abstraction.

The workflow here should be to write only what's needed now in a way that doesn't hinder further improvements. If, later on, more requirements come in, we can think about creating a base class, abstract some methods, and perhaps we will discover a design pattern that emerged for our solution.

#### Keep It Simple : KIS

This principle relates to the YAGNI principle. 

When you are designing a software component, avoid over-engineering it.
Ask yourself if your solution is the minimal one that fits the problem.

This design principle is an idea we will want to keep in mind at all levels of
abstraction, whether we are thinking of a high-level design, or addressing a
particular line of code.

Remember, trying to come up with generic interfaces when we didn't already find a pattern will only lead to even worse problems.

Recommendation : avoid advanced features of Python like meta-classes or meta-programming (99.9% of the time it will not be required, make the code harder to read and to maintain).

#### Easier to Ask Forgivness than Permission : EAFP

The idea of EAFP is that we write our code so that it performs an action
directly, and then we take care of the consequences later in case it doesn't
work. In python it's done using the workflow `try:except:finaly`.

It's the opposite of Look Before You Leap (LBYP) that first check what we are about to use.

In general code written using EAFP is more easy to read. 

### Final remarks

Good software design involves a combination of following good practices of software engineering and taking advantage of most of the features of the used language (in this case pythonic code).

There is great value in using everything that Python has to offer, but there is also a great risk of abusing this and trying to fit complex features into simple designs (over-engineering).

#### Orthogonality

In a perfect code, changing a module, class, or function should have no impact on the outside world to that component that is being modified (correspond to a coupling degree equal to 0) but it's not always possible. We should aim for this utopia where a good design will try to minimize the impact as much as possible.

For example we could use mixins to show how to make modules orthogonal between them. 

```python
class BaseTokenizer:
    def __init__(self, str_token):
        self.str_token = str_token
    def __iter__(self):
        yield from self.str_token.split("-")

class UpperIterableMixin:
    def __iter__(self):
        return map(str.upper, super().__iter__())

class Tokenizer(UpperIterableMixin, BaseTokenizer):
    pass
```


The fact that the `__iter__` method returned a new generator increases the chances that all three classes (the base, the mixing, and the concrete class) are orthogonal.

If this had returned something in concrete (a list , let's say), this would have created a dependency on the rest of the classes, because when we changed the list to something else, we might have needed to update other parts of the code, revealing that the classes were not as independent as they should be.

There is an interesting quality aspect that relates to orthogonality. If two parts of the code are orthogonal, it means one can change without affecting the other. This implies that the part that changed has unit tests that are also orthogonal to the unit tests of the rest of the application. Under this assumption, if those tests pass, we can assume (up to a certain degree) that the application is correct without needing full regression testing !

More broadly, orthogonality can be thought of in terms of features. Two functionalities of the application can be totally independent so that they can be tested and released without having to worry that one might break the other. 

#### Structuring the code

The way code is organized impacts the performance of the team and its maintainability. In particular, having large files with lots of definitions is a bad practice and should be discouraged. A good code base will structure and arrange components by similarity.

Even if other multiple parts of the code depend on definitions made on that file, this can be broken down into a package, and will maintain total compatibility. 

The idea would be to create a new directory with a `__init__.py` file on it (this will make it a Python package). Alongside this file, we will have multiple files with all the particular definitions each one requires then the `__init__.py` file will import from all the other files the definitions it previously had and additionally, these definitions can be mentioned in the `__all__` variable of the module to make them exportable.

There are many advantages to this : 

1. It contains fewer objects to parse and load into memory when the module is imported.
2. The module itself will probably be importing fewer modules because it needs fewer dependencies, like before.

Centralizing information in this way (like constants) makes it easier to reuse code and helps to avoid inadvertent duplication. (More details about separating modules and creating Python packages later in context of software architecture.)


## The SOLID Principle

We will review the SOLID principles and how to implement them in a Pythonic way. These principles entail a series of good practices to achieve better-quality software.

* __S__ ingle Responsability Principle : a software component must have only one responsibility
* __O__ pen/closed principle : a module should be both open to extension and closed to modification
* __L__ iskov's substitution principle : there is a series of properties that an object type must hold to preserve the reliability of its design
* __I__ nterface segregation principle : provides some guidelines to make interfaces small (duck typing)
* __D__ ependency inversion principle : design principle by which we protect our code by making it independent of things that are fragile, volatile, or out of our control. we want to force whatever implementation or detail to adapt to our code via a sort of API.

An interesting observation of most (if not all) of the principles that we will explore in this chapter is that we shouldn't try to get them right from the very first design. The idea is to design software that can be easily extended and changed, and that can evolve toward a more stable version.

### Single Responsabilty Principle 

The single responsibility principle states that a software component (in general, a class) must have only one responsibility. It's closely related to the idea of cohesion in software design. 

If a class has a sole responsibility then it means that it is in charge of doing just one concrete thing. As a consequence of that, we can conclude that it must have only one reason to change ! 

If we have to make modifications to a class for different reasons, it means the abstraction is incorrect, and that the class has too many responsibilities. This is probably an indication that there is at least one abstraction missing. 

We can see this principle under two point of view : 

1. Design class in such a way that most of their properties and their attributes are used by their methods, most of the time. In this case we know they are related concepts and we can group them under the same abstraction.

2. If, when looking at a class, we find methods that are mutually exclusive and do not relate to each other, then they are the different responsibilities that have to be broken down into smaller classes.


We will study an example where a class has too many responsibilities (God object) and how to applies the Single Responsabilty principle in this case. 

#### Problem : A class with too many responsibilities 

TODO : change the example i don't like it


We want to create a case for an application that is in charge of reading information about events from a source (ex : log files, a database, or many more sources), and identify the actions corresponding to each particular log.


Example of a class that fails to conform to the SRP. 

```python
class SystemMonitor:
    def load_activity(self):
        """Get the events from a source, to be processed."""
    def identify_events(self):
        """Parse the source raw data into events"""
    def stream_events(self):
        """Send the parsed events to an external agent."""
```

We can see that is doesn't conform to the SRP based on the 2nd view point. 

This class defines an interface with a set of methods that corresponds to actions that are orthogonal/independant between them. 

Consider the loader method. It's goal is to retrieves the informations (events) from a source (log, db, ...). 
We can abstractly define the requiered steps to perform the loading : 

1. Connect to data source 
2. Load data 
3. Parse in corresponding format 
...

Ok. Now imagine if we want to modify how the data is stored (for example from list to dictionnary). Then we will need to make changes in the `SystemMonitor` class. Should a class named `SystemMonitor` have the responsability of defining which data structure to use for retrieving the informations ? The answer is NO. It should not change because we changed the representation of it's data. 

The same reasoning applies to other modification and the other methods of this class. This class doesn't conform to the SRP and it's quite difficult to maintain it. There are lots of different reasons that will impact changes in this class. Instead, we want external factors to impact our code as little as possible.

Here the cohesion of the class is small and the coupling is high. That's clearly a bad design !

#### Solution : Distributing responsabilies

The solution, again, is to create smaller and more cohesive abstractions. We will therefore separate every method into a different class.
Now each class will have a single responsability (Monitor:identify, Output:stream, Watch:load)

Instead of using God object we will instead use a object that interact with instances of the classes managing independently a single responsability. 


The idea now is that changes to any of these classes do not impact the rest, and all of them have a clear and specific meaning. Changes are local, the impact is minimal, and each class is easier to maintain.

If we go back to the previous issue when modifying how we store the data, now the alert system is not even aware of these changes, so we do not have to modify anything on the system monitor ! 

Note : The new classes define interfaces that are not only more maintainable but also reusable. One important clarification is that the principle does not mean at all that each class must have a single method. Any of the new classes might have extra methods, as long as they correspond to the same logic that that class is in charge of handling.


```python
class ActivityWatch:
    def load(self):
        pass 

class SystemMonitor:
    def identify(self, event):
        pass 

class Stream:
    def stream(self, events):
        pass 

class AlertSystem:
    monitor = SystemMonitor()
    watcher = ActivityWatch()
    output = Stream()

    def run(self):
        pass
```

Remember we shouldn't try to get the SRP right from the very first design (more important if we start specifying the responsabilites). 
The SRP can be used as a thought process, for example we want to design a component (a class) and there are a lot of different things that need to be done (multiple responsabilites). We know that we should separate reponsabilites but what are the right boundaries to separate these responsibilities ???? 

To be able to separate and not lose time on deep thinking on how to separate them we can start writing a monolithic class in order to understand what the internal collaborations are and how responsibilities are distributed. This will help us get a clearer picture of the new abstractions that need to be created.


### Open/Closed Principle 

When designing a class (or any component) we should carefully encapsulate the implementation details, so that it has good maintenance. We want the component to be open to extension (to adapt to new requirements or changes in the domain problem) but closed to modification (we only
want to add new things to our component, not change anything existing that is closed to modification.)

If for some reason, when something new has to be added we find ourselves modifying the code, then that logic is probably poorly designed.


#### Problem : Maintainability perils 

We will resume with our previous use case. The idea is that we have a part of the system that is in charge of identifying events as they occur in another system, which is being monitored. At each point, we want this component to identify the type of event, correctly, according to the values of the data that was previously gathered. 


An example of design to perform this task could be the following one that is no closed to modification : 

```python
@dataclass
class Event:
    raw_data: dict

class UnknownEvent(Event):
    """A type of event that cannot be identified from its data."""

class LoginEvent(Event):
    """A event representing a user that has just entered the system"""

class LogoutEvent(Event):
    """An event representing a user that has just left the system"""

class SystemMonitor:
    """Identify events that occurred in the system."""
    def __init__(self, event_data : dict):
        self.event_data = event_data
    def identify_event(self):
        if (
        self.event_data["before"]["session"] == 0
        and self.event_data["after"]["session"] == 1
        ):
            return LoginEvent(self.event_data)
        elif(
        self.event_data["before"]["session"] == 1
        and self.event_data["after"]["session"] == 0
        ):
            return LogoutEvent(self.event_data)
        return UnknownEvent(self.event_data)
```

Ok so we can identify with this design 2 types of Events : Login and Logout of a User. Now if we want to extend the design to be able to identify a new Event how should we proceed ?

At first glance this design seems open to extension : adding a new event would be about creating a new subclass of Event , and then the system monitor should be able to work with them, but if we want to identify it we will need to make modification within the method used in the system monitor class.

Note : You've may be noticed that when we don't know which type of Event we are dealing with we return a UnknownEvent instead of a None value. This is to preserve polymorphism by following the `null` object pattern (more on that later when dealing with common design patterns). 



Ok let's identify the flaws of this design and why it doesn't agrees to the Open/Closed principle. 


The first issue that is quite easy to identify is that the logic for determining the types of events is centralized inside a monolithic method. As the number of events we want to support grows, this method will as well, and it could end up being a very long method (not following the Single Responsability Principle).

Another issue is that the `identify_event` method is not closed to modification when we want to extend a new type of Events for example. 
Every time we want to add a new type of event to the system, we will have to change something in this method (not to mention that the
chain of elif statements will be a nightmare to read!).

#### Solution : Refactoring the event system for extensibility 

Fondamentaly the issue with the previous design was that the SystemMonitor class was interacting directly with the concrete classes it was going to retrieve (Kezako ?). We recieve in this class an event_data and we make analysis on it !

In order to achieve a design that honors the open/closed principle, we have to design towards abstractions. A possible solution would be to let the SystemMonitor class collaborates with the events, and delegate the logic for identifying each particular type of event to its corresponding class. 

Then we have to add a new (polymorphic) method to each type of event with the single responsibility of determining if it corresponds to the data being passed or not, and we also have to change the logic to go through all events, finding the right one (the code is way more cleaner and readable this way !!!)

```python
class Event:
    def __init__(self, raw_data):
        self.raw_data = raw_data
    @staticmethod 
    def meets_condition(event_data: dict) -> bool: # Could be an abstraction and this method could be implemented in UnknownEvent
        return False

class UnknownEvent(Event):
    """A type of event that cannot be identified from its data"""
class LoginEvent(Event):
    @staticmethod
    def meets_condition(event_data: dict):
        return (
            event_data["before"]["session"] == 0
            and event_data["after"]["session"] == 1
        )
class LogoutEvent(Event):
    @staticmethod
    def meets_condition(event_data: dict):
        return (
            event_data["before"]["session"] == 1
            and event_data["after"]["session"] == 0
        )

class SystemMonitor:
    """Identify events that occurred in the system."""
    def __init__(self, event_data):
        self.event_data = event_data
    def identify_event(self):
        for event_cls in Event.__subclasses__():
            try:
                if event_cls.meets_condition(self.event_data):
                    return event_cls(self.event_data)
            except KeyError:
                continue
        return UnknownEvent(self.event_data)
```

With this design the interaction is now oriented towards an abstraction (the generic base class `Event`). The method `identify_event` now works just with generic events that follow a common interface (polymorphic with respect to the `meets_condition` method.). Events are discovered through the `__subclasses__()` method. Supporting new types of events is now just about creating a new class for that event that has to extend Event and implement its own `meets_condition()` method.

Note : There is not only one possible design but the idea remain the same : here we relies on the `__subclasses__()` method but we could use other alternatives such as registering classes using the `abc` module, or creating our own registry !


Ok so this design is clearly open to extension but is it closed to modification ? If we want to add a new type of Event we don't need to modify anything !!! We can just create a new class and that's it ! It's the same if we want to remove a type of event for example.


With this example we can notice that the OCP is closely related to the effective use of polymorphism to solve the issue of maintainability. We want to work towards designing abstractions that respect a polymorphic contract that the client can use, to a structure that is generic enough that extending the model is possible, as long as the polymorphic relationship is preserved.

The perils of not following the OCP are ripple effects and problems in the software where a single change triggers changes all over the code base, or risks breaking other parts of the code.

Note : in order to achieve this design we need to be able to create proper closure against the abstractions we want to protect (here new type of Events). This is not always possible (abstractions might collide, we might have a proper abstraction that provides closure against a requirement but does not work for other types of requirements). In these cases, we need to be selective and apply a strategy that provides the best closure for the types of requirements that require being the most extensible ! 

### Liskov's Substitution Principle (this principle seems too restrictive to use in practice)

The main idea behind this principle is that, for any class, a client should be able to use any of its subtypes indistinguishably without compromising the expected behavior at runtime (without even noticing that another subtype as been used). 

The original definition states : if S is a subtype of T, then objects of type T may be replaced by objects of type S, without breaking the program.

Imagine that there is some client class and we want this client to interact with objects of some type, it will work through an
interface.

What this type should be ? This type might as well be just a generic interface definition, an abstract class or an interface, NOT a class with the behavior itself. 
The idea behind this principle is that if the hierarchy is correctly implemented, the client class has to be able to work with instances of any of the subclasses without even noticing. These objects should be interchangeable. 


This principle relates to the ideas behind designing by contract, there is a contract between a given type and a client. (A good class must define a clear and concise interface)



We can detect simple case of LSP violation by using type annotations and linter. 

But there are more subtle cases where it's not so clear or obvious and we have to rely upon careful code inspection when doing a code review.


Cases where contracts are modified are particularly harder to detect automatically (add a new method too a subclasses or a more restrictive constraint/validation on it). 

REMEMBER the entire idea of LSP is that subclasses can be used by clients just like their parent class. In the case of contract world a subclass can never make preconditions stricter than they are defined on the parent class and a subclass can never make postconditions weaker than they are defined on the parent class. 


We take back our example from OCP and we will improve and adapt it too respect LSP.  


```python
class Event:
    def __init__(self, raw_data):
        self.raw_data = raw_data
    @staticmethod 
    def meets_condition(event_data: dict) -> bool: # Could be an abstraction and this method could be implemented in UnknownEvent
        return False
    
    @staticmethod
    def validate_precondition(event_data: dict):
        """
        Precondition of the contract of this interface.
        Validate that the ``event_data`` parameter is properly defined
        """
        if not isinstance(event_data, Mapping):
            raise ValueError(f"{event_data} is not a dict")
        for moment in ("before", "after"):
            if moment not in event_data:
                raise ValueError(f"{moment} not in {event_data}"
            if not isinstance(event_data[moment], Mapping):
                raise ValueError(f"{event_data[moment]} is not a dict"

class UnknownEvent(Event):
    """A type of event that cannot be identified from its data"""
class LoginEvent(Event):
    @staticmethod
    def meets_condition(event_data: dict):
        return (
            event_data["before"].get("session") == 0
            and event_data["after"].get("session") == 1
        )
class LogoutEvent(Event):
    @staticmethod
    def meets_condition(event_data: dict):
        return (
            event_data["before"].get("session") == 1
            and event_data["after"].get("session") == 0
        )

class SystemMonitor:
    """Identify events that occurred in the system."""
    def __init__(self, event_data):
        Even.validate_precondition(event_data)
        self.event_data = event_data

    def identify_event(self):
        event_cls = next(
            (
                clevent_cls 
                for event_cls in Event.___subclasses__() 
                if event_cls.meets_condition(self.event_data)
            ),
            UnknownEvent
        )
        return event_cls(self.event_data)
        
```


Here the contract only states that the top-level keys `before` and `after` are mandatory and that their values should also be dictionaries.
If we want to make preconditions stricter for one subclasse of Event we CAN'T. 

The only workaround is to use Substitution by value in the method meet_condition ! 

For example if we want to create a TransactionEvent we can't enforce the appearance of the 'transaction' key in the dictionnary because it will be stricter than the parent class Event. However we can just use Substitution by value to have a workaround. 


```python
class TransactionEvent(Event):
    @staticmethod
    def meets_condition(event_data: dict):
        return event_data["after"].get("transaction") is not None
```

The LSP is fundamental to good object-oriented software design because it emphasizes one of its core traits which is polymorphism. 
It is about creating correct hierarchies so that classes derived from a base one are polymorphic along the parent one. 

The LSP is closely related to DbC and OCP. If we attempt to extend a class with a new one that is incompatible, it will fail, the contract with the client will be broken, and as a result such an extension will not be possible. To make it work (by force) we will need to make modification and that's contrary to the OCP. 


### Interface Segregation Principle


The interface segregation principle (ISP) provides some guidelines for the idea that interfaces should be small.

First let's define what is a interface. In object-oriented terms, an interface is represented by the set of methods and properties (fields) an object exposes. The interface separates the definition of the exposed behavior for a class from its implementation.

In Python, interfaces were implicitly defined by a class according to its methods because off the duck typing principle (any object is really
represented by the methods it has, and by what it is capable of doing : "If it walks like a duck, and quacks like a duck, it must be a duck.")

PEP-3119 introduced the concept of abstract base class to define interfaces in a new way. This basic idea is to define a basic interface (set of methods) that some derived classes are responsible for implementing. This is useful in situations where we want to make sure that certain critical methods are actually overridden, and it also works as a mechanism for overriding or extending the functionality of methods. 

The `abc` module also contains a way of registering some types as part of a hierarchy, in what is called a virtual subclass. The idea is that this extends the concept of duck typing a little bit further by adding a new criterion — walks like a duck, quacks like a duck, or... it says it is a duck.

By separating interfaces into the smallest possible units, to favor code reusability, each class that wants to implement one of these interfaces will most likely be highly cohesive given that it has a quite definite behavior and set of responsibilities.


#### Problem : An interface that provides too much

We will resume our System monitoring example where we left it. This time we want to extend the data sources. 
We want to be able to parse an event from several data sources, in different formats : JSON and XML. 

A first try would be to create a interface like this

```python
class EventParser(metaclass = ABCMeta):
    @abstractmethod
    def from_xml(self):
        raise NotImplemented

    @abstractmethod
    def from_json(self):
        raise NotImplemented
```

Events that derive from this abstract base class and implement these methods would be able to work with their corresponding types.
However we can have events that does not need the XML method, and can only be constructed from a JSON. If we keep this design this particular event would still carry the `from_xml()` method from the interface, and since it does not need it, it will have to pass it.

This is creates coupling and forces clients of the interface to work with methods that they do not need, so we can assume that this interface provides too much ! 

#### Solution : The smaller the interface, the better


A solution here would be to separate this interface into two different interfaces, one for each method.

```python

class EventParserXML(metaclass = ABCMeta):
    @abstractmethod
    def from_xml(self):
        ...

class EventParserJSON(metaclass = ABCMeta): 
    @abstractmethod
    def from_json(self):
        ...

class EventParser(EventParserJSON, EventParserXML):
    def from_xml(self):
        """actual implementation"""
        pass

    def from_json(self):
        """actual implementation"""
        pass
```


With this design, objects that derive from `EventParserXML` and implement the `from_xml()` method will know how to be constructed from an XML,
and the same for a JSON file. More importantly we maintain the orthogonality of two independent functions (from_json and from_xml are just related by the fact that they are method to extract event from source, but if the json structure changes there is no reason to change the xml method so the interface is coupled for nothing !), and preserve the flexibility of the system without losing any functionality that can still be achieved by composing new smaller objects.

There is some resemblance to the SRP, but the main difference is that here we are talking about interfaces, so it is an abstract definition of behavior.


Rq : This way to code will make the codebase long and reusable and we will need at a point to decouple them into multiples libraries or even independant services. 


#### Caution : DO NOT GO TO THE EXTREME

The point made in the previous section is valid : The smaller the interface, the better. We could reformulate it The smaller and the more cohesive the interface, the better. 

A base class (abstract or not) defines an interface for all the other classes to extend it. The fact that this should be as small as possible relates to the fact that it should do one thing. That doesn't mean it must necessarily have one method !


### Dependency Inversion Principle

This principle is one of the most powerful one and is closely related to the subject of common design patterns and clean architecture. 

The goal of this principle is to make the code independent of things that are fragile, volatile, or out of our control (ex : 3rd library, module developed by another team, ...). 

The basic idea is to INVERT dependecies. Normaly our code depends on external dependencies and their concrete implementations but the goal to this principle is to invert the dependecies. The external dependencies should depend on our code and we achieve this throught interfaces (abstractions)


Let's take an exemple : Imagine that two objects in our design need to collaborate, A (ex : our code) and B (ex : 3rd party library). A works with an instance of B (so A depends on B and can't control it, if A depends heavily on it a new version of B may break A). 

The idea of the DIP as we said is to invert the dependecies so to make B adapt to A. The concrete way to do it is to present an interface and forcing B to comply to this interface. This way our code will not depend on the concerte implementation provided by B but will depend on the interface we defined. 

This principle is backed by the hypothesis that we expect concrete implementation to change more frequently than abstract components. So interfaces can be used as flexible points where we can expect the system to evolve without having to change the abstraction itself. 


#### Problem : A case of rigid dependencies 

Let's resume with the example of the Even's monitoring system where we left it. 

We want after having identified the event type to deliver it to a data collector where we will further analyse it (for analytics purpose for example)

An idea could be to implement a EventStreamer that will stream whatever data that is being delivred to and create a concrete Syslog class to send the data a log files. 

```python

class Syslog:
    def send():
        """Concrete implementation"""

class EventStreamer:
    delivery = Syslog()
    def stream(self, data):
        delivery.send(processed_data)
```

In fact this is a bad design. EventStreamer is a high level concept class and it depends on a low level class Syslog (a concrete implementation). For example if we want to changes something on how we send the data to syslog we will modify EventStreamer. 

If we want to modify or add new data destination (database, slack channel, ...) we will also need to modify the stream method. 

This proves that the design is not good and needs to be improved. 



#### Solution : Invert the dependencies


Our EventStreamer (a high level concept) was depending on Syslog (a low level concept, concrete implementation). 

The proposed solution using the Dependency Inversion Principle is to work with an interface (high level concept) rather than the concrete implementation. 


```python
class DataTargetClient(ABC): #Abstract class
    def send(self, event):
        ... #Abstract

class Syslog(DataTargetClient):
    def send(self, event):
        """Concrete implementation"""

class EventStreamer:
    def __init__(self, target : DataTargetClient):
        self.target = target
    
    def stream(self, events : list[Event]):
        for event in events:
            self.target.send(event.serialize())
```

Notice how the dependencies have now been inverted. EventStreamer does not depend on a concrete implementation of a
particular data target and doesn't care about it. What it cares about is the interface provided by DataTargetClient. 

HUGE NOTICE : In this case we understand the importance of the LSP. If we want to add a new target (Database for example) we need for it to comply with the abstraction and can't add new methods or be more restrictive. The reason is because the EventStreamer will only work with interface and nothing else. It's the same for the event that what ever the type of it should define a serialize method.


Remarque : What we used to write the python code is called dependency injection. We modified the target to be used at runtime (dynamic injection throught the init method). The only thing we asked in the class definition was to have a target that is a DataTargetClient
To be more specific python doesn't care about the typing as it's not a typed language at runtime and every object with a send method could be used as a target but it's a good practice to code this way and to have polymorphism available. 
Remeber : Object that implements a send method (If it walks like a duck and quacks like a duck it must be a duck) vs class extending the abstract class (If it's says it's a duck then it IS A duck !). In our case we KNOW FOR SURE that the Syslog is a DataTargetClient because it tells us that it is a DataTargetClient. 

The injection thing is really just a way of making objects collaborate between them. We could either fix them as constant or use inject (parameters). Globally sticking with injection is better as it simplify testing and enables polymorphism. 


#### More on dependency injection 

Try to never for the creation of dependecies in the init method (pain in the ass to test and don't enables the correct use of polymorphism). 
We want in general to let users of the object to define themselves the dependencies (that should conform to a contract) and use them a arguments in the init method. 


We can have some cases where the initialisation starts to become complex (more arguments) or we have a lot of object to initialize (in this case it's not a design problem ???????? God object ????). 

A good idea could be to define the interaction/collaboration of objects using a dependecy graph (trop stylé) and it's the job of a library to use this dependecy graph to make the creation of objects (remove the boilerplate of the glue code that is required to bind the different objects)

We can use for example the library pinject provided by Google (archived) or dependency-injector (last update 1 year ago). 

```python


class EventStreamer:
    def __init__(self, target : DataTargetClient):
        self._target = target
    
    def stream(self, events : list[Event]):
        for event in events:
            self._target.send(event.serialize())

class _EventStreamerBindingSpec(pinject.BindingSpec): #object that knows how the dependencies are injected; here the Syslog
    def provide_target(self): # any method named as provide_<dependency> should return the dependency with that name as the suffix
        return Syslog()

# We create the graph object which we will use to get objects with the dependencies already provided
object_graph = pinject.new_object_graph(
    binding_specs = [_EventStreamerBindingSpec()]
)

#Finaly we can instanciate our event_stremer object 

event_streamer = object_graph.provide(EventStreamer) #Here target will be an instance of Syslog
```

When you have multiple dependencies or interrelations among objects, it's probably a good idea to write them declarative and let a tool handle the initialization for you (similar to a factory object in the sense we define how they're created in a single place and the tool do it for us). 



### Summary 

The SOLID principles are key guidelines for good object-oriented software design and are necessary because building software is an incredibly hard task. Remember if we get the design wrong, it will cost us a lot in the future (complexe, rigid code logic, behavior at runtime impossible to predict, can't adapt to requirements change). 

Even with a good design there is no way to determine if our design will be correct and if our software will be flexible and adaptable for years to come. The only solution is to stick to a set of principles that had proved theirs values over time and SOLID principles can help us. They are not magical or perfect but they provide good guidelines to follow. 


Remember : The idea isn't to get all the requirements right from the very first version, but to achieve a design that's extensible and flexible enough to change, so that we can adapt it as needed.