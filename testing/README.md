# Summary of python testing with pytest

## Definitions

### Type of test

1. Unit test: A test that checks a small piece of code, such as a function or a class, in isolation from the rest of the system.

2. Integration test: A test that checks a larger portion of the code, possibly several classes or a subsystem. It's mostly a label used for a test larger than a unit test but smaller than a system test.

3. System test (end-to-end): A test that checks the entire system under test in an environment as close to the end-user environment as possible.

4. Functional test: A test that checks a single piece of functionality of a system.

5. Subcutaneous test: A test that doesn't run against the final end-user interface but against an interface just below the surface.


### Outcomes of test

1. PASSED (.): The test ran successfully.

2. FAILED (F): The test did not run successfully (or XPASS + strict).

3. SKIPPED (s): The test was skipped. You can tell pytest to skip a test by using either the
@pytest.mark.skip() or pytest.mark.skipif() decorators​.

4. xfail (x): The test was not supposed to pass, ran, and failed. You can tell pytest that a test
is expected to fail by using the @pytest.mark.xfail() decorator.

5. XPASS (X): The test was not supposed to pass, ran, and passed.

6. ERROR (E): An exception happened outside of the test function, in either a fixture or in a hook function.

### Type of fixture scope

1. Function : Run once per test function. The setup portion is run before each test using the fixture. The
teardown portion is run after each test using the fixture.

2. Class : Run once per test class

3. Module : Run once per module

4. Session : Run once per session. All test methods and functions using a fixture of session scope share
one setup and teardown call.

### Miscellaneous 

1. Fixture : Elements or preconditions set up to ensure a consistent and reproducible testing environment.


## Pytest arguments 

Here's the most useful arguments to use : 

1. `-k EXPRESSION` : use an expression to find what test functions to run. Can be used with keywords `and`, `or` and `not`.
2. `--collect-only` : shows you which tests will be run with the given options and configuration.
3. `-s`: allows any output that normally would be printed to stdout to actually be printed while the tests are running
4. `--lf` : run just the failing previous tests
5. `-l` : local variables and their values are displayed with tracebacks for failing tests.
6. `--durations=N` : reports the slowest N number of tests/setups/teardowns after the tests run (N=0 for all)

More for debugging 

* --tb=[auto/long/short/line/native/no]: Controls the traceback style.
* -x / --exitfirst: Stops the tests session with the first failure.
* --pdb: Starts an interactive debugging session at the point of failure.


## How to write tests 

There's plenty of ways to write and optimize tests 

1. Assert : `assert something`
2. Exception : `with pytest.raises(your_exception)` 
3. Marking : `@pytest.mark.your_mark_name`. 
Exemple : Smoke tests (test if there is some major break in the system). Use with `pytest -m your_mark_name`.
This argument can be used with keywords `and`, `or` and `not`. 
4. Fixture : `@pytest.fixture(scope)`. Can be use to initialise and clean up an object through the scope of execution
5. Skip : `@pytest.mark.skip(reason)` to skip test. If for example you want to skip test that doesn't respect a condition you can use `@pytest.mark.skipif(condition, reason)`
6. xFail : `@pytest.mark.xfail` to run a test function, but that we expect it to fail.
7. Parametrize : `@pytest.mark.parametrize(argnames,argvalues)`
7. Test class : way to group tests that make sense to be grouped together. Nothing much to add. 

### Fixtures 

To share fixtures among multiple test files, you need to use a `conftest.py`. 
A fixture function runs before the tests that use it.
If there is a yield in the function, it stops there, passes control to the tests, and picks up on the next line after the tests are done.
You can also return data with the yield if needed. 
1. Setup and Teardown : Can be seen in terminal using `--setup-show` argument 

```
@pytest.fixture()
​def​ db():
​"""Connect to db before tests, disconnect after."""​
​# Setup : start db​

​yield​ ​# this is where the testing happens​

​# Teardown : stop db​
```

2. Specifying Fixtures with usefixtures or arguments : A test using a fixture due to usefixtures cannot use the fixture’s return value.
If you want a fixture to run all of the time and be available as a value use autouse=True when defining fixtures. 
```
@pytest.mark.usefixtures(’fixture1’, ’fixture2’)
def my_test():
    ...
```
or 
```
def my_test(fixture1, fixture2):
    ...
```

3. Parametrizing Fixtures : 

```
@pytest.fixture(params=['psgsql', 'mysql'])
​def​ db(request):
​"""Connect to db before tests, disconnect after."""​
​# Setup : start db​ with name request.param

​yield​ ​# this is where the testing happens​

​# Teardown : stop db​
```

### Builtin Fixtures 

The builtin fixtures that come prepackaged with pytest can help you do some pretty useful things
in your tests easily and consistently

1. `tmpdir` and `tmpdir_factory`: create a temporary file system directory before your test runs, and remove the directory when your test is finished. You can use `tmpdir` to create files or directories used by a single test, and you can use `tmpdir_factory` when you want to set up a directory for many tests. The `tmpdir` fixture has function scope, and the `tmpdir_factory` fixture has session scope.

2. `pytestconfig` : can control how pytest runs through command-line arguments and options, configuration files, plugins, and the directory from which you launched pytest (shortcut to request.config)

Example : 

```
def​ pytest_addoption(parser): #A hook builtin 
    parser.addoption(​"--myopt"​, action=​"store_true"​, help=​"some boolean option"​)
    parser.addoption(​"--foo"​, action=​"store"​, default=​"bar"​, help=​"foo: bar or baz"​)
```
`pytest --myopt --foo baz` will thrn set the arguments `myopt` to `True` and `foo` to `baz`. 
To access these options you can use the builtin fixture `pytestconfig` and use `pytestconfig.getoption('option_name')`

3. `cache` : sometimes passing information from one test session to the next can be quite useful. The cache fixture is all about storing information about one test session and retrieving it in the next. The cache fixture has function scope (shortcut to request.config.cache)

4. `capsys` : allows you to retrieve stdout and stderr from some code, and it disables output capture temporarily

5. `monkeypatch` : A “monkey patch” is a dynamic modification of a class or module during runtime. The monkeypatch builtin fixture allows you to take over part of the runtime environment of the code under test and replace either input dependencies or output dependencies with objects or functions that are more convenient for testing. (See Mock chapter)

6. `doctest_namespace`

7. `recwarn` : builtin fixture used to examine warnings generated by code under test.

### Plugins and how to create them

What's a plugin in pytest world ? Any time you put fixtures and/or hook functions into a project’s top-level conftest.py file, you created a local conftest plugin. You can then convert these conftest.py files into installable shareable plugins. Plugins can include hook functions that alter pytest’s behavior.

Why use plugins ? handful of fixtures that you want to share between a couple of projects can be shared easily by
creating a plugin.

Pytest plugins are installed with pip, just like other Python packages.

Look at pytest hook and plugins ! Not essential for now this part


### Configuration 

1. `pytest.ini`: This is the primary pytest configuration file that allows you to change default behavior. You can get a list of all the valid settings for pytest.ini from pytest --help

```
[pytest]
minversion = ​3.0​
addopts = ​-rsxX -l --tb=short --strict​
xfail_strict = ​true​
markers =
​smoke:​ ​Run​ ​the​ ​smoke​ ​test​ ​functions​ ​for​ ​tasks​ ​project​
​get:​ ​Run​ ​the​ ​test​ ​functions​ ​that​ ​test​ ​tasks.get()​
​...​ ​more​ ​options​ ​...​
```
xfail_strict = true causes tests marked with @pytest.mark.xfail that don’t fail to be reported as an error. (should always be set)
addopts set the options you want for testing, you don’t have to type them in anymore (here --strict to only allow declared markers) 
markers allows you to mark a subset of tests to run with a specific marker. You can now also see them with pytest --markers

2. `conftest.py`: This is a local plugin to allow hook functions and fixtures for the directory where the conftest.py file exists and all subdirectories.
3. `__init__.py`: When put into every test subdirectory, this file allows you to have identical test filenames in multiple test directories.


Test discovery traverses many directories recursively. But there are some directories you just know you don’t want pytest looking in.
The default setting to tells pytest where not to look is `norecursedirs = .* build dist CVS _darcs {arch} and *.egg`. 

In contrary testspaths is a list of directories relative to the root directory to look in for tests. It’s only used if a directory, file, or nodeid is not given as an argument.

Now that pytest knows in which folder to search, it needs to finds tests to run based on certain test discovery rules. The standard discovery rules are 

* A test module is a file with a name that looks like test_*.py or *_test.py.
* Look in test modules for functions that start with test_.
* Look for classes that start with Test. Look for methods in those classes that start with test_ but don’t have an __init__ method.

You can change the pattern to select from (not recommended) using 

```
[pytest]
python_files = ​test_* *_test check_*​
python_classes = ​*Test Test* *Suite​
python_functions = ​test_* check_*​
```

Having `__init__.py` files in all of your test subdirectories, you can have the same test filename show up in multiple directories. Running them individually
will be fine, but running pytest from the dups directory won’t work. 


### Pytest with other tools 

1. pdb : The pdb module is the Python debugger in the standard library. You use --pdb to have pytest start a debugging session at the point of failure.

Now that we are at the (Pdb) prompt, we have access to all of the interactive debugging features of pdb. 

* p/print expr: Prints the value of exp.
* pp expr: Pretty prints the value of expr.
* l/list: Lists the point of failure and five lines of code above and below.
* l/list begin,end: Lists specific line numbers.
* a/args: Prints the arguments of the current function with their values. (This is helpful when
* in a test helper function.)
* u/up: Moves up one level in the stack trace.
* d/down: Moves down one level in the stack trace.
* q/quit: Quits the debugging session.


2. pytest-cov : Code coverage is a measurement of what percentage of the code under test is being tested by a
test suite. You need to install the plugin pytest-cov and you can then get a coverage report using `pytest​​ ​--cov=src​​ ​--cov-report=html​ `


While code coverage tools are extremely useful, striving for 100% coverage can be dangerous.
When you see code that isn’t tested, it might mean a test is needed. **But it also might mean that there’s some functionality of the system that isn’t needed and could be removed.** Like all software development tools, code coverage analysis does not replace thinking.


### mock: Swapping Out Part of the System

The mock package is used to swap out pieces of the system to isolate bits of our code under test from the rest of the system. Mock objects are sometimes called test doubles, spies, fakes, or stubs. Between pytest’s own monkeypatch fixture and mock, you should have all the test double functionality you need.

The mock package is shipped as part of the Python standard library as unittest.mock. 

Tests that use mocks are necessarily white-box tests, and we have to look into the code to decide what to mock and where.

Example : We want to mock a context manager used to connect to a DB. 

*tasks/cli.py*
```
@contextmanager
​def​ _tasks_db():
    config = tasks.config.get_config()
    tasks.start_tasks_db(config.db_path, config.db_type)
    ​yield​
    tasks.stop_tasks_db()
```

We want to mock it with an empty context manager. 

```
@contextmanager
​def​ mock_tasks_db():
    ​yield​
```

To do this we will use the mocker fixture provided by the pytest-mock plugin as a convenience interface to unittest.mock.
We can use it like this `mocker.patch.object(tasks.cli, ​'_tasks_db'​, new=mock_tasks_db)`. If we want to mock the returned value of a function we could use `mocker.patch.object(tasks.cli.tasks, ’list_tasks’, return_value=[])`. It will replaces any calls to the function as a default MagicMock object with a return value of an empty list.

Need to more details of mocking. (Improve this tuto using Clean Code in Python - Unit Testing And Refactoring)